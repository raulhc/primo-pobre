/*

  Raul Hacker Club
  
 */

#include <Servo.h>
 
// Servos info
#define LEFT_SERVO_ZERO 96
#define RIGHT_SERVO_ZERO 94

#define LEFT_SERVO_PIN 9 
#define RIGHT_SERVO_PIN 10

// Servo variables
Servo servo_left;
Servo servo_right;


/* Message Definition
   The Arduino expects this message to send the controls for the motor
  {*, left_speed, right_speed, no_of_cycles, #}
  */

#define MESSAGE_START_CHAR '*'
#define MESSAGE_END_CHAR '#'

char inputBuffer[3];         // a string to hold incoming data
enum messageStates{READY, FIRST_BYTE, SECOND_BYTE, THIRD_BYTE, END_OF_MESSAGE, DONE};
int messageState = READY;  // whether the string is complete
char no_of_cycles = 0, left_speed = 0, right_speed = 0;




void setup() {
  
  servo_left.attach(LEFT_SERVO_PIN);
  servo_right.attach(RIGHT_SERVO_PIN);
  
  servo_left.write(LEFT_SERVO_ZERO); 
  servo_right.write(RIGHT_SERVO_ZERO);
  
  // initialize serial:
  Serial.begin(57600);

}

void loop() {
  // print the string when a newline arrives:
  if(messageState == DONE) {
    // Do robot stuff
    Serial.print("left_speed: ");
    Serial.println(left_speed);
    
    Serial.print("right_speed: ");
    Serial.println(right_speed);
    
    Serial.print("no_of_cycles: ");
    Serial.println(no_of_cycles);
    
    left_speed = (LEFT_SERVO_ZERO - 25)+ ( 5 * (left_speed - 0x30)); // ASCII code to servo angle in a range of 0 - 180 degrees 20 by 20 (just for testing, don't worry!)
    right_speed = (RIGHT_SERVO_ZERO - 25)+ ( 5 * (right_speed - 0x30)); // ASCII code to servo angle in a range of 0 - 180 degrees 20 by 20 (just for testing, don't worry!)
    
    servo_left.write(left_speed); 
    servo_right.write(right_speed);
    
    messageState = READY;
  }
}

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read(); 
    switch (messageState){
      case(READY):
        if (inChar == MESSAGE_START_CHAR) messageState = FIRST_BYTE;
        break;
      case (FIRST_BYTE):
        left_speed = inChar; // Char in the range 0 - 9
        messageState = SECOND_BYTE;
        break;
      case (SECOND_BYTE):
        right_speed = inChar; // Char in the range 0 - 9
        messageState = THIRD_BYTE;
        break;
      case (THIRD_BYTE):
        no_of_cycles = inChar; // Char in the range 0 - 9
        messageState = END_OF_MESSAGE;
        break;
      case (END_OF_MESSAGE):
        if (inChar == MESSAGE_END_CHAR) messageState = DONE;
        else messageState = READY;
        break;   
    }
  }
}

